package rip.deadcode.ratpack.is;

import ratpack.server.RatpackServer;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

public final class Sample {

    public static void main( String[] args ) throws Exception {

        RatpackServer.start( spec -> {
            spec.serverConfig( config -> config
                    .development( true ) )
                .handlers( chain -> {
                    chain.all( ctx -> {
                        InputStream is = new ByteArrayInputStream( "foo".getBytes( StandardCharsets.UTF_8 ) );
                        InputStreamHelper.send( ctx.getResponse(), is );
                    } );
                } );
        } );
    }
}
