package rip.deadcode.ratpack.is;

import io.netty.handler.codec.http.HttpChunkedInput;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.stream.ChunkedStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ratpack.http.Response;
import ratpack.http.Status;
import ratpack.http.internal.DefaultResponse;
import ratpack.server.internal.DefaultResponseTransmitter;

import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.List;

public final class InputStreamHelper {

    private static final Logger logger = LoggerFactory.getLogger( InputStreamHelper.class );

    public static void send( Response response, InputStream is ) {

        if ( !response.getClass().equals( DefaultResponse.class ) ) {
            throw new RuntimeException( "Compatibility error." );
        }

        try {
            sendChecked( response, is );
        } catch ( Exception e ) {
            throw new RuntimeException( e );
        }
    }

    public static void sendChecked( Response response, InputStream is ) throws Exception {

        Method finalizeResponse = response.getClass().getDeclaredMethod( "finalizeResponse", Iterator.class, Runnable.class );
        finalizeResponse.setAccessible( true );
        Field responseFinalizers = response.getClass().getDeclaredField( "responseFinalizers" );
        responseFinalizers.setAccessible( true );
        Method setCookieHeader = response.getClass().getDeclaredMethod( "setCookieHeader" );
        setCookieHeader.setAccessible( true );
        Field status = response.getClass().getDeclaredField( "status" );
        status.setAccessible( true );

        finalizeResponse.invoke(
                response,
                ( (List) responseFinalizers.get( response ) ).iterator(),
                (Runnable) () -> {
                    try {
                        setCookieHeader.invoke( response );
                        transmit( response, ( (Status) status.get( response ) ).getNettyStatus(), is );
                    } catch ( Exception e ) {
                        throw new RuntimeException( e );
                    }
                }
        );
    }

    private static void transmit( Response response, HttpResponseStatus status, InputStream is ) throws Exception {

        Field responseTransmitter = response.getClass().getDeclaredField( "responseTransmitter" );
        responseTransmitter.setAccessible( true );

        DefaultResponseTransmitter transmitter = (DefaultResponseTransmitter) responseTransmitter.get( response );

        Method transmit = transmitter.getClass().getDeclaredMethod(
                "transmit", HttpResponseStatus.class, Object.class, boolean.class );
        transmit.setAccessible( true );
        transmit.invoke( transmitter, status, new HttpChunkedInput( new ChunkedStream( is ) ), false );
    }

}
